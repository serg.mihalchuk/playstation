package com.example.playStation;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestPlayStation {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                "applicationContext.xml");

        PlayStation playStation = context.getBean("playStation", PlayStation.class);
        playStation.playGame(GameGenre.ACTION);
        playStation.playGame(GameGenre.SPORT);


//        PlayStation playStation = context.getBean("playStation", PlayStation.class);
//        playStation.playGame();


//        Games secondPlayStation = context.getBean("sportGames", Games.class);
//        PlayStation sportGames = new PlayStation(secondPlayStation);
//        sportGames.playGames();
//        Games thirdPlayStation = context.getBean("tableGames", Games.class);
//        PlayStation tableGames = new PlayStation(thirdPlayStation);
//        tableGames.playGames();

        context.close();

    }

}
