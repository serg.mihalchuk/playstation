package com.example.playStation;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SportGames implements Games{
    private List<String> sportGame = new ArrayList<>();

    // Блок инициализации объекта
    // Выполняется каждый раз, когда создается объект класса
    {
        sportGame.add("Fifa");
        sportGame.add("NHL");
        sportGame.add("NBA");

    }

    @Override
    public List<String> getGame() {
        return sportGame;
    }
}
