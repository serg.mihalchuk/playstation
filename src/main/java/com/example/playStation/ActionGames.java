package com.example.playStation;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ActionGames implements Games{
    private List<String> actionGame = new ArrayList<>();

    // блок инициализации
    {
        actionGame.add("FarCry");
        actionGame.add("Doom");
        actionGame.add("GTA");

    }

    @Override
    public List<String> getGame() {
        return actionGame;
    }
}
