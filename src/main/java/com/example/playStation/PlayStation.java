package com.example.playStation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class PlayStation {
    private ActionGames actionGames;
    private SportGames sportGames;

    @Autowired
    public PlayStation(ActionGames actionGames, SportGames sportGames) {
        this.actionGames = actionGames;
        this.sportGames = sportGames;
    }

    public void playGame(GameGenre genre) {
        Random random = new Random();

        // случайное целое число между 0 и 2
        int randomNumber = random.nextInt(3);

        if (genre == GameGenre.ACTION) {
            // случайная Action игра
            System.out.println(actionGames.getGame().get(randomNumber));
        } else {
            // случайная Sport игра
            System.out.println(sportGames.getGame().get(randomNumber));
        }
    }


//        System.out.println("You are playing: " + sportGames.getGame());
//        System.out.println("You are playing: " + tableGames.getGame());
}

//    private List<Games> gameList = new ArrayList<>();


//    public void setGameList(List<Games> gameList) {
//        this.gameList = gameList;
//    }


//    public void chooseGamesList() {
//        for(Games game : gameList) {
//            System.out.println("You can play: " + game.getGame());
//        }





